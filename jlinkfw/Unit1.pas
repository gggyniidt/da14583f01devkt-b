unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    Button2: TButton;
    LabeledEdit4: TLabeledEdit;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure LabeledEdit2Exit(Sender: TObject);
    procedure LabeledEdit4Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

  dlg: topendialog;
  fss, fsd: TFileStream; { 声明一个文件流 }
  sp, dp: string;
  s, l: integer;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);

begin
  dp := ExtractFilePath(application.ExeName) + 'JLinkARM.bin';

  s := StrToInt(LabeledEdit1.Text);
  l := StrToInt(LabeledEdit4.Text);

  fss := TFileStream.Create(sp, fmOpenRead or fmShareExclusive);
  fsd := TFileStream.Create(dp, fmCreate);

  fss.Position := s; { 流指针移到开始, 复制时从这里开始 }
  fsd.CopyFrom(fss, l); { Copy 流 }
  { CopyFrom 的参数 2 是要复制的内容大小; 如果为 0 , 不管指针在什么位置都会复制所有内容 }
  { CopyFrom 返回实际拷贝的字节数 }

  fss.Free;
  fsd.Free;
end;

procedure TForm1.Button2Click(Sender: TObject);

begin
  dlg := topendialog.Create(application);
  dlg.Execute(self.Handle);
  sp := dlg.FileName;

  if not FileExists(sp) then
  begin
    ShowMessage('找不到文件');
    exit;
  end;

  LabeledEdit3.Text := sp;

end;

procedure TForm1.LabeledEdit2Exit(Sender: TObject);
var
  s:integer;
begin
  s:=strtoint(LabeledEdit2.Text)-strtoint(LabeledEdit1.Text)+1;
  LabeledEdit4.Text:= format('0x%X',[s]);
end;

procedure TForm1.LabeledEdit4Exit(Sender: TObject);
var
  e:integer;
begin
  e:=strtoint(LabeledEdit1.Text)+strtoint(LabeledEdit4.Text)-1;
  LabeledEdit2.Text:=format('0x%X',[e]);
end;

end.
